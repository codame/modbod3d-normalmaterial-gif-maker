var scene;
var renderer;
var camera;
var sphere;
var mesh;
var rotationFrame;
var helper;

function setScene(){
	// set the scene size
	var WIDTH = 400,
		HEIGHT = 400;

	// set some camera attributes
	var VIEW_ANGLE = 45,
		ASPECT = WIDTH / HEIGHT,
		NEAR = 0.1,
		FAR = 10000;

	// get the DOM element to attach to
	// - assume we've got jQuery to hand
	var $container = $('#container');

	// create a WebGL renderer, camera
	// and a scene
	renderer = new THREE.WebGLRenderer();
	camera = new THREE.PerspectiveCamera(
		VIEW_ANGLE,
		ASPECT,
		NEAR,
		FAR
	);

	scene = new THREE.Scene();

	// add the camera to the scene
	scene.add(camera);

	// the camera starts at 0,0,0
	// so pull it back
	camera.position.z = 300;
	camera.position.y = 150;

	// start the renderer
	renderer.setSize(WIDTH, HEIGHT);

	// attach the render-supplied DOM element
	$container.append(renderer.domElement);
}

function makeSphere(){
	// set up the sphere vars
	var radius = 5,
	    segments = 16,
	    rings = 16;

	var sphereMaterial = new THREE.MeshNormalMaterial();

	// create a new mesh with
	// sphere geometry - we will cover
	// the sphereMaterial next!
	sphere = new THREE.Mesh(

	  new THREE.SphereGeometry(
	    radius,
	    segments,
	    rings),

	  sphereMaterial);

	// add the sphere to the scene
	scene.add(sphere);	

}

function setControls(){
	//var radius = sphere.geometry.boundingSphere.radius;
	controls = new THREE.OrbitControls( camera );
	controls.target = new THREE.Vector3( 0, 0, 0 );
	//controls.noPan = true;
	//controls.noRotate = true;
	controls.update();
}

function render() {
	var time = Date.now() * 0.001;


	if(mesh){
		mesh.rotation.z = time * -5;
		//mesh.rotation.y = time * 5;		
		//parent_node.rotation.z = time * 0.5;
	}
	
	renderer.render( scene, camera );

}

function loadModel(){
	
	var loader = new THREE.STLLoader();

	loader.addEventListener( 'load', function ( event ) {
		var material = new THREE.MeshNormalMaterial();
		var geometry = event.content;
		//modifying the center point
		//geometry.applyMatrix( new THREE.Matrix4().makeTranslation( 0, 0, 0 ) );
		mesh = new THREE.Mesh( geometry, material );


		//003_jordanG-calm.stl
		/*
		mesh.position.set( 0, -60, 0);
		//if a model is loaded sideways, get it upright
		mesh.rotation.set( - Math.PI / 2, 0,  0);
		mesh.scale.set( 2, 2, 2 );
		*/

		mesh.position.set( 20, -100, 0);
		//if a model is loaded sideways, get it upright
		mesh.rotation.set( - Math.PI / 2, 0,  0);
		mesh.scale.set( 130, 130, 130 );

		/*
		//clayon-bust.stl
		mesh.position.set( 0, 0, 0);
		//if a model is loaded sideways, get it upright
		mesh.rotation.set( - Math.PI / 2, 0,  0);
		mesh.scale.set( 20, 20, 20 );
		*/

		scene.add( mesh );

	} );
	//loader.load( 'stl/003_jordanG-calm.stl' );
	loader.load( 'stl/tiare-rides-jordan-no-walls-origin.stl' );
	//loader.load( 'stl/clayton-head2.stl' );
}

function animate(){
	requestAnimationFrame( animate, renderer.domElement );
	render();
}

$( document ).ready(function() {
	// init
	setScene();

	// draw
	//makeSphere();
	loadModel();
	animate();

	// interaction
	setControls();
});
